const servicejwt = require("../services/servicejwt");

const auth = function(req, res, next) {
	console.log(req.headers.authorization);
	if (!req.headers.authorization) {
		return res.status(403).send({ message: "No tienes permiso" });
	}
	const token = req.headers.authorization.split(" ")[1];
	try {
		payload = servicejwt.decodeToken(token);
	} catch (error) {
		return res.status(401).send(`${error}`);
	}
	next();

	//res.status(200).send({ message: "con permiso" });
};

//router.use(auth);

//para proteger una sóla ruta
// router.get("/", auth, (req, res) => {
// 	productController.index(req, res);
// });

// //o así
// router.get('/', auth, productController.index})

module.exports = {
	auth
};
