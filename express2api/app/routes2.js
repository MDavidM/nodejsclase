const express = require("express");
// para establecer las distintas rutas, necesitamos instanciar el express router
var router = express.Router();
const routerCervezas = require("./routes/v2/cervezas.js");
const routerProducts = require("./routes/v2/products.js");
const routerUsers = require("./routes/v2/users.js");
const routerCategorias = require("./routes/v2/categorias.js");


//establecemos nuestra primera ruta, mediante get.
router.get("/", (req, res) => {
	res.json({ mensaje: "¡Bienvenido a nuestra API! con mongodb" });
});
//prueba ambar
//para la ruta
//const Cerveza = require("./models/v2/Cerveza");

// router.get("/ambar", (req, res) => {
//   const miCerveza = new Cerveza({ nombre: "Ambar" });
//   miCerveza.save((err, miCerveza) => {
//     if (err) return console.error(err);
//     console.log(`Guardada en bbdd ${miCerveza.nombre}`);
//   });
// });

router.use("/cervezas", routerCervezas);
router.use("/products", routerProducts);
router.use("/users", routerUsers);
router.use("/categorias",routerCategorias );

module.exports = router;
