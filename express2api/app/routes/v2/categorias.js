const express = require("express");
const router = express.Router();
const categoriasController = require("../../controllers/v2/categoriasController.js");
const servicejwt = require("../../services/servicejwt");
const auth = require("../../middleware/auth");

router.use(auth.auth);

//rutas
router.get("/", (req, res) => {
	console.log("ruta de productos");
	categoriasController.index(req, res);
});

// router.get("/", auth, (req, res) => {
// 	productController.index(req, res);
// });

router.get("/:id", (req, res) => {
	console.log("estas en show");
	categoriasController.show(req, res);
});

router.post("/", (req, res) => {
	console.log("estas en create");
	categoriasController.create(req, res);
});

router.delete("/:id", (req, res) => {
	console.log("estas en delete");
	categoriasController.remove(req, res);
});

router.put("/:id", (req, res) => {
	console.log("estas en update");
	categoriasController.update(req, res);
});

module.exports = router;
