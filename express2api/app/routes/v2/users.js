const express = require("express");
const router = express.Router();

const userController = require("../../controllers/v2/userController.js");
//registro de un nuevo usuario
router.post("/", userController.register);

// router.get("/", (req, res) => {
// 	console.log("ruta de productos");
// 	userController.index(req, res);
// });

// router.get("/:id", (req, res) => {
// 	console.log("estas en show");
// 	userController.show(req, res);
// });

// router.post("/", (req, res) => {
// 	console.log("estas en create");
// 	userController.create(req, res);
// });

// router.delete("/:id", (req, res) => {
// 	console.log("estas en delete");
// 	userController.remove(req, res);
// });

// router.put("/:id", (req, res) => {
// 	console.log("estas en update");
// 	userController.update(req, res);
// });

module.exports = router;
