
var express = require('express')
const router = express.Router()
const cervezaController = require('../controllers/cervezaController.js');
router.get('/', (req, res) => {
    cervezaController.index(req, res)
})


router.get('/', (req, res) => {
    cervezaController.index(req, res)
    // res.json({ mensaje: `¡Bebiendo la cerveza: ${req.params.id}!` })
})

router.get('/:id', (req, res) => {
    cervezaController.show(req, res)
    // res.json({ mensaje: `Cerveza creada: ${req.body.nombre}` })
})

router.post('/', (req, res) => {
    cervezaController.store(req, res)
    // res.json({ mensaje: `Cerveza creada: ${req.body.nombre}` })
})

router.delete('/:id', (req, res) => {
    cervezaController.destroy(req, res)
    // res.json({ mensaje: 'Cerveza borrada!' })
})

router.put('/:id', (req, res) => {
    cervezaController.update(req, res)
    // res.json({ mensaje: 'Cerveza actualizada!' })
})

module.exports = router
