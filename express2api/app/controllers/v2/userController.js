// const mongoose = require("mongoose");
const User = require("../../models/v2/User");
const servicejwt = require("../../services/servicejwt");
const register = (req, res) => {
	const user = new User({
		email: req.body.email,
		name: req.body.name,
		password: req.body.password
	});

	user.save(err => {
		if (err)
			res.status(500).send({ message: `Error al crear usuario: ${err}` });
		// servicejwt nos va a crear un token
		return res.status(200).send({ token: servicejwt.createToken(user) });
	});
};

module.exports = {
	register
};
