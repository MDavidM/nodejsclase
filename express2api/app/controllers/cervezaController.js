const connection = require('../config/dbconnection')
const Cerveza = require('../models/Cerveza')

const index = function (req, res) {
  // console.log('Lista de cervezas')

  const cervezas = Cerveza.index(function (err, data, fields) {
    if (err) {
      res.status(500).json({ mensaje: 'error' })
    } else {
      res.status(200).json(data)
    }
  })
  // console.log(cervezas)
  // PROHIBIDO HACER res.json AQUÍ!!!!
  // res.json({ message: 'la hemos liao!' })
}

const show = function (req, res) {
  console.log('Detalle de cervezas')
  const id = req.params.id
  Cerveza.find(id, function (err, data) {
    if (err) {
      res.status(500).json({ mensaje: 'error' })
    } else if (!data.length) {
      res.status(404).json({ mensaje: 'not found' })
    } else {
      res.status(200).json(data)
    }
  })
  //   res.json({ mensaje: `Detalle de la cerveza: ${id}` })
  // res.json({ mensaje: `Lista de cervezas, página ${page}` })
}
const store = function (req, res) {
  console.log('Crear nueva cerveza')
  const cerveza = {
    name: req.body.name,
    container: req.body.container,
    price: req.body.price,
    alcohol: req.body.alcohol
  }

  Cerveza.create(cerveza, (err, data) => {
    if (err) {
      // res.status(500).json(err)
      res.status(500).json({ mensaje: 'falló la inserción' })
    } else {
      res.status(201).json(data)
    }
  })
  console.log(cerveza)
}

const update = function (req, res) {
  const cerveza = {
    id: req.params.id,
    name: req.body.name,
    container: req.body.container,
    price: req.body.price,
    alcohol: req.body.alcohol
  }
  Cerveza.update(cerveza, (err, data) => {
    if (err) {
      res.status(500).json(err)
    } else {
      res.json(data)
    }
  })
}
const destroy = function (req, res) {
  const id = req.params.id
  Cerveza.destroy(id, (err, data) => {
    if (err) {
      res.status(500).json({ mensaje: 'fallo' })
    } else {
      res.status(200).json({ mensaje: 'borrado' })
    }
  })
}

module.exports = {
  index,
  show,
  update,
  destroy,
  store
}
