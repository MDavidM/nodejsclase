const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema({
  nombre: {
    type: String,
    required: true,
    maxlength: 20
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    maxlength: 255
  },
  created: {
    type: Date,
    default: Date.now
  }
});

const Product = mongoose.model("Product", productSchema);

module.exports = Product;
