const express = require('express')

const router = express.Router()
const cervezaController = require('../controllers/cervezaController.js')

router.get('/', (req, res) => {
  cervezaController.index(req, res)
})

router.get('/:id', (req, res) => {
  cervezaController.show(req, res)
})

router.post('/', (req, res) => {
  cervezaController.store(req, res)
})

router.delete('/:id', (req, res) => {
  cervezaController.destroy(req, res)
})

router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
})

module.exports = router
