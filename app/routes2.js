const express = require("express");
// para establecer las distintas rutas, necesitamos instanciar el express router
var router = express.Router();
const routerCervezas = require("./routes/v2/cervezas.js");
const Cerveza = require("./models/v2/cervezas.js");
//establecemos nuestra primera ruta, mediante get.
router.get("/", (req, res) => {
  res.json({ mensaje: "¡Bienvenido a nuestra API! con mongodb" });
});

router.get("/ambar", (req, res) => {
  const miCerveza = new Cerveza({ nombre: "Ambar" });
  miCerveza.save((err, miCerveza) => {
    if (err) return console.error(err);
    console.log(`Guardada en bbdd ${miCerveza.nombre}`);
  });
});
//router.use("/cervezas", routerCervezas);

module.exports = router;
